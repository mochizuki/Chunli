package com.mochizuki.chunli;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

public class Chunli extends Actor implements Disposable {

	public static final String FILE_TEXTURE_1 = "images/pic1n.png";
	public static final String FILE_TEXTURE_2 = "images/pic2n.png";

	private final int FRAME_HEIGHT = 150;
	private final int FRAME_WIDTH = 180;

	public static final int ACTION_STAND = 1;
	public static final int ACTION_WALK_LEFT = 2;
	public static final int ACTION_WALK_RIGHT = 3;
	public static final int ACTION_HAND_ACCATK1 = 4;
	public static final int ACTION_HAND_ACCATK2 = 5;
	public static final int ACTION_HAND_ACCATK3 = 6;
	public static final int ACTION_HAND_ACCATK4 = 7;
	public static final int ACTION_HAND_ACCATK5 = 8;
	
	public static final int ACTION_FOOT_ACCATK1 = 9;
	public static final int ACTION_FOOT_ACCATK2 = 10;
	public static final int ACTION_FOOT_ACCATK3 = 11;
	public static final int ACTION_FOOT_ACCATK4 = 12;
	public static final int ACTION_FOOT_ACCATK5 = 13;
	public static final int ACTION_FOOT_ACCATK6 = 14;
	
	public static final int ACTION_JUMP = 15;
	public static final int ACTION_CROUCH = 16;
	public static final int ACTION_PRECROUCH = 17;
	// private final int picXcount = 10;
	// private final int picYcount = 12;

//	private Texture m_Texture1;
//	private Texture m_Texture2;
	private Animation m_AnimStand;
	private Animation m_AnimHandAccatk1;
	private Animation m_AnimWalkLeft;
	private Animation m_AnimWalkRight;
	private Animation m_AnimHandAccatk2;
	private Animation m_AnimCurrent;
	// private Animation m_CurrentAnim;

	private float m_stateTime;
	private Fire m_Fire;


	private boolean m_CanChangeAction;
	private Animation m_AnimHandAccatk3;
	private Animation m_AnimHandAccatk4;
	private Animation m_AnimHandAccatk5;
	private Animation m_AnimFootAccatk1;
	private Animation m_AnimFootAccatk2;
	private Animation m_AnimFootAccatk3;
	private Animation m_AnimFootAccatk4;
	private Animation m_AnimFootAccatk5;
	private Animation m_AnimFootAccatk6;
	private Animation m_AnimJump;
	private Animation m_AnimCrouch;
	private Animation m_AnimPreCrouch;

	public Chunli() {
		// TODO Auto-generated constructor stub
		// m_Texture = new Texture("images/pic1n.png");
		setWidth(FRAME_WIDTH*MainScreen.POWER_X);
		setHeight(FRAME_HEIGHT*MainScreen.POWER_Y);
	}

	public void dispose() {
		// TODO Auto-generated method stub
		// m_Texture.dispose();
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		m_stateTime += Gdx.graphics.getDeltaTime();

		TextureRegion currentFrame = m_AnimCurrent.getKeyFrame(m_stateTime);

		batch.draw(currentFrame, getX(), getY());
//		m_Fire.draw(batch, parentAlpha);
		if (!m_CanChangeAction && m_AnimCurrent.isAnimationFinished(m_stateTime)) {
			m_AnimCurrent = m_AnimStand;
			m_CanChangeAction = true;
		}

		// super.draw(batch, parentAlpha);
	}

	/**
	 * 根据起始序号和帧数得到帧序列
	 * 
	 * @param start
	 *            以0为起始
	 * @param end
	 * @return
	 */
	private Animation getAnim(Texture pic, int start, int count, float frameDuration) {
		Array<TextureRegion> regions = new Array<TextureRegion>();
		int picXcount = 10;
		// int picYcount=12;
		int x, y;
		TextureRegion region;
		for (int i = 0; i < count; i++) {
			x = start % picXcount;
			y = start / picXcount;
			region = new TextureRegion(pic, x * FRAME_WIDTH, y
					* FRAME_HEIGHT, FRAME_WIDTH, FRAME_HEIGHT);
			regions.add(region);
			start++;
		}
		return new Animation(frameDuration, regions);
	}


	public void doAction(int command) {
		if(!m_CanChangeAction)
			return;
		switch (command) {
		case ACTION_STAND:
			m_CanChangeAction = true;
			m_AnimCurrent = m_AnimStand;
			break;
		case ACTION_CROUCH:
			m_CanChangeAction = true;
			m_AnimCurrent=m_AnimCrouch;
			break;
		case ACTION_JUMP:
			m_stateTime=0;
			m_CanChangeAction = false;
			m_AnimCurrent=m_AnimJump;
			break;
		case ACTION_WALK_LEFT:
			m_CanChangeAction = true;
			m_AnimCurrent = m_AnimWalkLeft;
			break;
		case ACTION_WALK_RIGHT:
			m_CanChangeAction = true;
			m_AnimCurrent = m_AnimWalkRight;
			break;
		case ACTION_HAND_ACCATK1:
			m_stateTime=0;
			m_CanChangeAction = false;
			m_AnimCurrent = m_AnimHandAccatk1;
			break;
		case ACTION_HAND_ACCATK2:
			m_stateTime=0;
			m_CanChangeAction = false;
			m_AnimCurrent = m_AnimHandAccatk2;
			break;
		case ACTION_HAND_ACCATK3:
			m_stateTime=0;
			m_CanChangeAction = false;
			m_AnimCurrent = m_AnimHandAccatk3;
			break;
		case ACTION_HAND_ACCATK4:
			m_stateTime=0;
			m_CanChangeAction = false;
			m_AnimCurrent = m_AnimHandAccatk4;
			break;
		case ACTION_HAND_ACCATK5:
			m_stateTime=0;
			m_CanChangeAction = false;
			m_AnimCurrent = m_AnimHandAccatk5;
			break;
		case ACTION_FOOT_ACCATK1:
			m_stateTime=0;
			m_CanChangeAction = false;
			m_AnimCurrent = m_AnimFootAccatk1;
			break;
		case ACTION_FOOT_ACCATK2:
			m_stateTime=0;
			m_CanChangeAction = false;
			m_AnimCurrent = m_AnimFootAccatk2;
			break;
		case ACTION_FOOT_ACCATK3:
			m_stateTime=0;
			m_CanChangeAction = false;
			m_AnimCurrent = m_AnimFootAccatk3;
			break;
		case ACTION_FOOT_ACCATK4:
			m_stateTime=0;
			m_CanChangeAction = false;
			m_AnimCurrent = m_AnimFootAccatk4;
			break;
		case ACTION_FOOT_ACCATK5:
			m_stateTime=0;
			m_CanChangeAction = false;
			m_AnimCurrent = m_AnimFootAccatk5;
			break;
		case ACTION_FOOT_ACCATK6:
			m_stateTime=0;
			m_CanChangeAction = false;
			m_AnimCurrent = m_AnimFootAccatk6;
			break;
		default:
			m_AnimCurrent = m_AnimStand;
		}
	}


	public void initResource(AssetManager asset) {
		Texture texture1 = asset.get(FILE_TEXTURE_1, Texture.class);
		Texture texture2 = asset.get(FILE_TEXTURE_2, Texture.class);
		m_AnimStand = getAnim(texture1,0, 10, 0.2f);
		m_AnimWalkRight = getAnim(texture1,11, 16, 0.25f);
		m_AnimWalkLeft = getAnim(texture1,26, 16, 0.25f);
		
		m_AnimHandAccatk1 = getAnim(texture1,62, 7, 0.2f);
		m_AnimHandAccatk2 = getAnim(texture1,70, 13, 0.2f);
		m_AnimHandAccatk3 = getAnim(texture1,98, 10, 0.2f);
		m_AnimHandAccatk4 = getAnim(texture2,0, 15, 0.2f);
		m_AnimHandAccatk5 = getAnim(texture2,32, 12, 0.2f);
		
		m_AnimFootAccatk1 = getAnim(texture2,15, 7, 0.2f);
		m_AnimFootAccatk2 = getAnim(texture2,23, 9, 0.2f);
		m_AnimFootAccatk3 = getAnim(texture2,44, 25, 0.2f);
		m_AnimFootAccatk4 = getAnim(texture2,69, 8, 0.2f);
		m_AnimFootAccatk5 = getAnim(texture2,77, 13, 0.2f);
		m_AnimFootAccatk6 = getAnim(texture2,90, 25, 0.2f);
		
		m_AnimJump = getAnim(texture1,57, 5, 0.08f);
		m_AnimCrouch = getAnim(texture2,117, 6, 0.2f);
		m_AnimPreCrouch = getAnim(texture2,114, 3, 0.3f);

		m_AnimStand.setPlayMode(Animation.LOOP);
		m_AnimWalkRight.setPlayMode(Animation.LOOP);
		m_AnimWalkLeft.setPlayMode(Animation.LOOP);
		m_AnimHandAccatk1.setPlayMode(Animation.NORMAL);
		m_AnimHandAccatk2.setPlayMode(Animation.NORMAL);
		m_AnimHandAccatk3.setPlayMode(Animation.NORMAL);
		m_AnimHandAccatk4.setPlayMode(Animation.NORMAL);
		m_AnimHandAccatk5.setPlayMode(Animation.NORMAL);

		m_AnimFootAccatk1.setPlayMode(Animation.NORMAL);
		m_AnimFootAccatk2.setPlayMode(Animation.NORMAL);
		m_AnimFootAccatk3.setPlayMode(Animation.NORMAL);
		m_AnimFootAccatk4.setPlayMode(Animation.NORMAL);
		m_AnimFootAccatk5.setPlayMode(Animation.NORMAL);
		m_AnimFootAccatk6.setPlayMode(Animation.NORMAL);
		
		m_AnimJump.setPlayMode(Animation.NORMAL);
		m_AnimCrouch.setPlayMode(Animation.NORMAL);
		m_AnimPreCrouch.setPlayMode(Animation.NORMAL);
		
		m_AnimCurrent = m_AnimStand;
		m_CanChangeAction = true;
		
		m_Fire=new Fire();
		m_Fire.initResource(asset);
		MoveToAction moveFire = new MoveToAction();
		moveFire.setDuration(4f);
		moveFire.setPosition(Gdx.graphics.getWidth(), this.getY());
		m_Fire.addAction(moveFire);
	}
}
