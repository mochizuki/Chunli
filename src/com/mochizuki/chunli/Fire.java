package com.mochizuki.chunli;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

public class Fire extends Actor implements Disposable {

	public static final String FILE_TEXTURE_FIRE = "images/firen.png";
	private Animation m_AnimFire;
	private float m_stateTime;

	public Fire() {
		// TODO Auto-generated constructor stub
		m_stateTime = 0;
		this.setWidth(69);
		this.setHeight(46);
	}

	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		super.draw(batch, parentAlpha);
		m_stateTime += Gdx.graphics.getDeltaTime();
		TextureRegion currentFrame = m_AnimFire.getKeyFrame(m_stateTime);
		batch.draw(currentFrame, getX(), getY());

	}

	public void initResource(AssetManager asset) {
		Texture texture = asset.get(FILE_TEXTURE_FIRE, Texture.class);
		Array<TextureRegion> regions = new Array<TextureRegion>();
		TextureRegion region;
		for (int i = 0; i < 10; i++) {
			region = new TextureRegion(texture,69*i,0,69,46);
			regions.add(region);
		}
		m_AnimFire = new Animation(0.3f, regions, Animation.LOOP);
	}
}
