package com.mochizuki.chunli;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Timer;

public class MainScreen implements Screen {

	private final String FILE_BACKGROUND = "images/back.png";
	private final String FILE_FONT = "fonts/chnfont.fnt";
	private final String FILE_PAD_BACKGROUND = "images/touchpadbk.png";
	private final String FILE_PAD_KNOB = "images/touchpadknob.png";

	private Stage m_Stage;
	private Chunli m_Chuli;
	private Fire m_Fire;
	private AssetManager m_AssetManager;
	private Label m_LabelFPS;
	private BitmapFont m_Font;
	private SpriteBatch m_Batch;
	private ProgressBar m_ProgressBar;
	private boolean m_initFinsh;
	// private Mesh m_Mesh;
	private TextureRegion m_rage;

	public static float POWER_X, POWER_Y;
	private float SCREEN_WIDTH, SCREEN_HEIGHT;

	private final int MOVE_NONE = 0;
	private final int MOVE_LEFT = 1;
	private final int MOVE_RIGHT = 2;
	private final int MOVE_UP = 4;

	private Timer m_moveTimer;

	public MainScreen() {
		// TODO Auto-generated constructor stub
	}

	public void dispose() {
		// TODO Auto-generated method stub
		m_AssetManager.clear();
		m_AssetManager.dispose();
		m_ProgressBar.dispose();
		m_Batch.dispose();
		m_Stage.dispose();

	}

	public void hide() {
		// TODO Auto-generated method stub

	}

	public void pause() {
		// TODO Auto-generated method stub

	}

	public void render(float arg0) {
		// TODO Auto-generated method stub
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		Gdx.gl.glClearColor(0f, 0f, 0f, 0f);

		if (m_LabelFPS != null)
			m_LabelFPS.setText("FPS:" + Gdx.graphics.getFramesPerSecond());
		if (m_rage != null) {
			m_Batch.begin();
			m_Batch.draw(m_rage, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
			m_Batch.end();
		}
		m_Stage.act(Gdx.graphics.getDeltaTime());
		m_Stage.draw();
		if (!m_AssetManager.update()) {
			m_Batch.begin();
			m_ProgressBar.draw(m_Batch, arg0);
			m_Batch.end();
			m_ProgressBar.setProgress(m_AssetManager.getProgress() * 100);
		}
		if (!m_initFinsh && m_AssetManager.update()) {
			PrepairStage();
			m_Chuli.initResource(m_AssetManager);
			m_Fire.initResource(m_AssetManager);
			m_initFinsh = true;
			// m_stage.removeActor(m_ProgressBar);
		}

	}

	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	public void resume() {
		// TODO Auto-generated method stub

	}

	public void show() {
		// TODO Auto-generated method stub
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		SCREEN_HEIGHT = Gdx.graphics.getHeight();
		POWER_X = SCREEN_WIDTH / 800f;
		POWER_Y = SCREEN_HEIGHT / 480f;

		m_AssetManager = new AssetManager();
		m_AssetManager.load(FILE_FONT, BitmapFont.class);
		m_AssetManager.load(FILE_BACKGROUND, Texture.class);
		m_AssetManager.load(Chunli.FILE_TEXTURE_1, Texture.class);
		m_AssetManager.load(Chunli.FILE_TEXTURE_2, Texture.class);
		m_AssetManager.load(Fire.FILE_TEXTURE_FIRE, Texture.class);
		m_AssetManager.load(FILE_PAD_BACKGROUND, Texture.class);
		m_AssetManager.load(FILE_PAD_KNOB, Texture.class);
		m_initFinsh = false;
		m_Stage = new Stage(SCREEN_WIDTH, SCREEN_HEIGHT, false);
		m_Batch = new SpriteBatch();
		m_ProgressBar = new ProgressBar(0, 0);

		m_Chuli = new Chunli();
		m_Fire = new Fire();
		// m_Chuli.setScale(POWER_X, POWER_Y);
		m_Chuli.setY(60 * POWER_Y);
		m_Chuli.setX((SCREEN_WIDTH - m_Chuli.getWidth()) / 2f);
		m_moveTimer = new Timer();
		m_moveTimer.scheduleTask(m_moveTask, 0, 0.05f);
		Gdx.input.setInputProcessor(m_Stage);
	}

	private void PrepairStage() {
		// // 创建一个静态的，由两个三角形构成的4个顶点的矩形
		// // 矩形分为3*2块。网格有3个位置参数(x,y,z)
		// // 网格的位置属性有两个值
		// m_Mesh = new Mesh(true, 4, 6, new VertexAttribute(
		// VertexAttributes.Usage.Position, 3, "attr_Position"),
		// new VertexAttribute(Usage.TextureCoordinates, 2,
		// "attr_texCoords"));
		// m_Mesh.setVertices(new float[] { -2048f, -2048f, 0, 0, 1, 2048f,
		// -2048f, 0, 1, 1, 2048f, 2048f, 0, 1, 0, -2048f, 2048f, 0, 0, 0 });
		//
		// // 设置索引，这个有点纠结，看文章详解
		// m_Mesh.setIndices(new short[] { 0, 1, 2, 2, 3, 0 });
		//
		// float WIDTH = Gdx.graphics.getWidth();
		//
		// float HEIGHT = Gdx.graphics.getHeight();
		//
		//
		//
		// cam = new OrthographicCamera(WIDTH, HEIGHT);
		//
		// cam.position.set(WIDTH / 2, HEIGHT / 2, 0);
		//
		//
		//
		// glViewport = new Rectangle(0, 0, WIDTH, HEIGHT);

		m_rage = new TextureRegion(m_AssetManager.get(FILE_BACKGROUND,
				Texture.class), 0, 0, 640, 480);
		m_Font = m_AssetManager.get(FILE_FONT, BitmapFont.class);
		LabelStyle ls = new LabelStyle();
		ls.font = m_Font;
		ls.fontColor = Color.RED;
		m_LabelFPS = new Label("FPS", ls);

		m_Stage.addActor(m_Chuli);
		m_Stage.addActor(m_Fire);
		m_Stage.addActor(m_LabelFPS);
		m_Stage.addActor(CreateTouchPad());
		createButtons();
	}

	private Touchpad CreateTouchPad() {
		ChangeListener listener = new ChangeListener() {


			@Override
			public void changed(ChangeEvent arg0, Actor arg1) {
				// TODO Auto-generated method stub
				Touchpad pad = (Touchpad) arg1;

//				if (Math.abs(pad.getKnobPercentX()) > 0.7
//						&& Math.abs(pad.getKnobPercentY()) > 0.7)
//					Log.i("PAD", "X:" + pad.getKnobPercentX() + "," + "Y:"
//							+ pad.getKnobPercentY());

				// 右移
				if (pad.getKnobPercentX() > 0.8) {
					if (m_Chuli.getActions() != null
							&& m_Chuli.getActions().size > 0)
						return;
					m_Chuli.doAction(Chunli.ACTION_WALK_RIGHT);
					// moveX.reset();
					// moveX.setPosition(SCREEN_WIDTH - m_Chuli.getWidth(),
					// m_Chuli.getY());
					// moveX.setDuration(10f * getPercentByPosition(true));
					// m_Chuli.addAction(moveX);
					if (m_CurrentMove == MOVE_NONE) {
						m_CurrentMove = MOVE_RIGHT;
						m_moveTimer.start();
					}
				}

				// 左移
				if (pad.getKnobPercentX() < -0.8) {
					if (m_Chuli.getActions() != null
							&& m_Chuli.getActions().size > 0)
						return;
					m_Chuli.doAction(Chunli.ACTION_WALK_LEFT);
					if (m_CurrentMove == MOVE_NONE) {
						m_CurrentMove = MOVE_LEFT;
						m_moveTimer.start();
					}
				}
				// 蹲下
				if (pad.getKnobPercentY() < -0.8)
					m_Chuli.doAction(Chunli.ACTION_CROUCH);
				// 跳
				if (pad.getKnobPercentY() > 0.8)
					m_Chuli.doAction(Chunli.ACTION_JUMP);

				// 回复
				if (Math.abs(pad.getKnobPercentX()) <= 0.7
						&& Math.abs(pad.getKnobPercentY()) <= 0.7) {
					m_Chuli.doAction(Chunli.ACTION_STAND);
					// m_Chuli.removeAction(moveX);
					m_CurrentMove = MOVE_NONE;
					m_moveTimer.stop();
				}
			}
		};

		TouchpadStyle tsy = new TouchpadStyle();

		Sprite sprite = new Sprite(m_AssetManager.get(FILE_PAD_BACKGROUND,
				Texture.class));
		// sprite.setScale(POWER_X);
		tsy.background = new SpriteDrawable(sprite);

		sprite = new Sprite(m_AssetManager.get(FILE_PAD_KNOB, Texture.class));
		// sprite.setScale(POWER_X);
		tsy.knob = new SpriteDrawable(sprite);

		Touchpad pad = new Touchpad(10f * POWER_X, tsy);
		pad.setX(50f * POWER_X);
		pad.setY(50f * POWER_Y);
		// pad.setScale(POWER_X);
		// pad.setWidth(128f*POWER_X);
		// pad.setHeight(pad.getWidth());
		pad.addListener(listener);
		return pad;
	}

	private void createButtons() {
		Texture texture = m_AssetManager.get(FILE_PAD_KNOB, Texture.class);
		Sprite sp = new Sprite(texture);
		SpriteDrawable sd = new SpriteDrawable(sp);
		final Button btnA = new Button(sd, sd);
		final Button btnB = new Button(sd, sd);
		final Button btnC = new Button(sd, sd);

		final Button btnX = new Button(sd, sd);
		final Button btnY = new Button(sd, sd);
		final Button btnZ = new Button(sd, sd);

		ClickListener listener = new ClickListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				if (event.getListenerActor() == btnA) {
					m_Chuli.doAction(Chunli.ACTION_FOOT_ACCATK1);
				}
				if (event.getListenerActor() == btnB) {
					m_Chuli.doAction(Chunli.ACTION_FOOT_ACCATK2);
				}
				if (event.getListenerActor() == btnC) {
					m_Chuli.doAction(Chunli.ACTION_FOOT_ACCATK3);
				}
				if (event.getListenerActor() == btnX) {
					m_Chuli.doAction(Chunli.ACTION_HAND_ACCATK1);
				}
				if (event.getListenerActor() == btnY) {
					m_Chuli.doAction(Chunli.ACTION_HAND_ACCATK2);
				}
				if (event.getListenerActor() == btnZ) {
					m_Chuli.doAction(Chunli.ACTION_HAND_ACCATK3);
				}
				return super.touchDown(event, x, y, pointer, button);
			}
		};

		btnA.setWidth(50f * POWER_X);
		btnA.setHeight(btnA.getWidth());

		btnB.setWidth(btnA.getWidth());
		btnB.setHeight(btnA.getHeight());

		btnC.setWidth(btnA.getWidth());
		btnC.setHeight(btnA.getHeight());

		btnX.setWidth(btnA.getWidth());
		btnX.setHeight(btnA.getHeight());

		btnY.setWidth(btnA.getWidth());
		btnY.setHeight(btnA.getHeight());

		btnZ.setWidth(btnA.getWidth());
		btnZ.setHeight(btnA.getHeight());

		btnA.setX(SCREEN_WIDTH - 185f * POWER_X);
		btnA.setY(30f * POWER_Y);
		btnB.setX(SCREEN_WIDTH - 125f * POWER_X);
		btnB.setY(btnA.getY());
		btnC.setX(SCREEN_WIDTH - 65f * POWER_X);
		btnC.setY(btnA.getY());

		btnX.setX(btnA.getX());
		btnY.setX(btnB.getX());
		btnZ.setX(btnC.getX());

		btnX.setY(btnA.getY() + btnA.getHeight() + 10f * POWER_Y);
		btnY.setY(btnX.getY());
		btnZ.setY(btnX.getY());

		btnA.addListener(listener);
		btnB.addListener(listener);
		btnC.addListener(listener);
		btnX.addListener(listener);
		btnY.addListener(listener);
		btnZ.addListener(listener);

		m_Stage.addActor(btnA);
		m_Stage.addActor(btnB);
		m_Stage.addActor(btnC);
		m_Stage.addActor(btnX);
		m_Stage.addActor(btnY);
		m_Stage.addActor(btnZ);
	}

	private int m_CurrentMove = MOVE_NONE;
	private Timer.Task m_moveTask = new Timer.Task() {
		float x, y;

		@Override
		public void run() {
			// TODO Auto-generated method stub
			x = m_Chuli.getX();
			y = m_Chuli.getY();
			float step = 5f * POWER_X;
			switch (m_CurrentMove) {
			case MOVE_NONE:
				return;
			case MOVE_LEFT:
				if (x > 0) {
					m_Chuli.setX(x - step > 0 ? x - step : 0);
				}
				break;
			case MOVE_RIGHT:
				float right = SCREEN_WIDTH - m_Chuli.getWidth();
				if (x < right) {
					//Log.i("moveright", "right="+right+",x="+x+",x + step="+(x + step));
					m_Chuli.setX(x + step < right ? x + step : right);
				}
				break;
			case MOVE_UP:
				m_Chuli.setY(y);
				break;
			}

		}
	};

}
